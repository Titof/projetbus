$(document).ready(function(){
    $('<input type="button" value="Voir sur une carte" id="mapButton"><div id="map-canvas"></div>').insertBefore('#map');
    $('#mapButton').click(loadMap);
});

function loadMap(){
    //Chargement API Google et de map.js
    google.load("maps", "3", {other_params:'sensor=false&key=AIzaSyCuqoeS1S74yFIR-C_KZtJXKCeuR0e9SqA',callback : function(){
        $.getScript("../Public/JS/map/map.js",function(){
            var lp = [{lat:-21.012, lng:55.27},{lat:-20.927116, lng:55.334902},{lat:-20.877712, lng:55.457312},{lat:-20.92, lng:55.49}]
            var mapp = new Mapp(lp); 
            mapp.initialize();
        });
    }});
    //On cache le boutton
    $('#mapButton').hide();
}