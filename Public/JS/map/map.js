var directionsDisplay;
var directionsService = new google.maps.DirectionsService();


function Mapp(listPoint){
    //On stocke les points recus.
    for(var i = 0; i < listPoint.length; i++){
        listPoint[i] = toLatLng(listPoint[i]);
    }
    this.depart = listPoint[0];
    this.arrive = listPoint[listPoint.length-1];
    this.intermediaire = listPoint.slice(1,listPoint.length-1);

    this.update = function(){
        //Mise en place des marqueurs
        this.departMarker = new google.maps.Marker({
            position: this.depart,
            map: this.map,
            //animation: google.maps.Animation.DROP,
            title: "Depart"
        });
        this.ArriveMarker = new google.maps.Marker({
            position: this.arrive,
            map: this.map,
            title: "Arrive"
        });
        this.intermediaireMarker = [];
        for(var i = 0; i < this.intermediaire.length; i++){
            this.intermediaireMarker[i]= new google.maps.Marker({
                position: this.intermediaire[i],
                map: this.map,
                title: "Arret Intermediaire"
            });
        }

        //Requete du chemin entre les arrets et affichage.
        var request = {
            origin:this.depart,
            destination:this.arrive,
            waypoints:adaptWayPoint(this.intermediaire),
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(result);
            }
        });
    }  
    this.initialize = function(){
        //Options google map
        var rendererOptions = {suppressMarkers : true}
        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
        var mapOptions = {
            center: this.depart,
            zoom: 10
        };
        this.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        directionsDisplay.setMap(this.map);
        this.update();
    }
}



function toLatLng(pos){
    return new google.maps.LatLng(pos.lat, pos.lng);
}
function adaptWayPoint(points){
    //Converti les arrets intermediaire en waypoints comprehensible pour la requete google du chemin
    var wayPoints = []
    for(var i = 0; i < points.length; i++){
        wayPoints[i] = {location: points[i], stopover:true};
    }
    return wayPoints;
}