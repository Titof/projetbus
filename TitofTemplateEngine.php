<?php
/*
Titof Template Engine V2

Novembre 2014

Moteur template avec gestion d'inclusion de blocs.

Inclusion de variable :
{{@nomdelavariable}}

Inclusion de bloc :
{{%block nomdubloc}}
*/


class Template
{

	protected $values = array();
	protected $file;

	public function __construct($file, $path="./")
	{
		$this->file = $file;
		$this->path = $path;
		$this->values = array();
	}

	function set($key, $value) {
		$this->values[$key] = $value;
	}


	function make() {
		// Chargement du fichier
		$tpl = file_get_contents($this->file, FILE_USE_INCLUDE_PATH);

		// Inclusion des blocs
		if (preg_match_all('/{{%block ([^}]*)}}/', $tpl, $matches) > 0) {

			for ($i=0; $i < count($matches[1]); $i++) { 
					$block = @file_get_contents($this->path.$matches[1][$i].".html", FILE_USE_INCLUDE_PATH);
					if ($block === FALSE) {
						echo "<br>ERROR : [".$matches[1][$i]."] Le fichier ".$this->path.$matches[1][$i].".html"." n'existe pas !<br>";
					}
					else {
						$tpl = preg_replace("/{{%block ".$matches[1][$i]."}}/", $block, $tpl);
					}
			}

		}

		// Transformation des variables

		foreach ($this->values as $key => $value) {
			//$rkey = "{{@".$key."}}";
			//$tpl = str_replace($rkey, $value, $tpl);
			$tpl = preg_replace("/{{@".$key."}}/", $value, $tpl);
		}


		echo($tpl);
	}
}

// Exemple :

$t = new Template('./test.html');
$t->set('nick', 'titof');
$t->make();


?>